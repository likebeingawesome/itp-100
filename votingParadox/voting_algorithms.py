def plurality(election):
    """
    Return the plurality winner among three candidates, 1, 2, and 3. The input
    election is a list of three tuples, so in a plurality election only the
    first element matters.

      >>> plurality([(1, 2, 3), (2, 3, 1), (2, 1, 3)])
      2
      >>> plurality([(3, 2, 1), (3, 2, 1), (2, 1, 3)])
      3
    """
    # Count the votes for each of the three candidates in a dictionary
    vote_totals = {1: 0, 2: 0, 3: 0}
    for vote in election:
        vote_totals[vote[0]] += 1

    # Return the candidate number with the highest vote count
    return max(vote_totals.items(), key=lambda x: x[1])[0]

def exhaustive(election):
    """
    Return the exhaustive winner among three candidates, 1, 2, and 3. The input
    election is a list of three tuples, so in a exhaustive election the first
    two elements matter.

      >>> exhaustive([(1, 2, 3), (2, 3, 1), (2, 1, 3)])
      2
      >>> exhaustive([(3, 2, 1), (3, 2, 1), (2, 1, 3)])
      3
    """
    # Count the votes for each of the three candidates in a dictionary
    vote_totals = {1: 0, 2: 0, 3: 0}
    for vote in election:
        vote_totals[vote[0]] += 1
    #declares the loser
    loser=1 if vote_totals[1]<vote_totals[2] and vote_totals[1]<vote_totals[3] else 2 if vote_totals[2]<vote_totals[3] and vote_totals[2]<vote_totals[1] else 3
    #Recounts without loser
    vote_totals = {1: 0, 2: 0, 3: 0}
    for vote in election:
        if vote[0] == loser:
            vote_totals[vote[1]] += 1
        else:
            vote_totals[vote[0]] += 1
    # Return the candidate number with the highest vote count
    return max(vote_totals.items(), key=lambda x: x[1])[0]
def twoStepPrimary(election, Num):
    vote_totals = {1: 0, 2: 0, 3: 0}
    for vote in election:
        if vote[0] == Num:
            vote_totals[vote[1]] += 1
        else:
            vote_totals[vote[0]] += 1
    #print("Vote totals:"+str(vote_totals))
    winner=1 if vote_totals[1]>vote_totals[2] and vote_totals[1]>vote_totals[3] else 2 if vote_totals[2]>vote_totals[3] and vote_totals[2]>vote_totals[1] else 3
    #print("Initial Winner:"+str(winner))
    vote_totals = {1: 0, 2: 0, 3: 0}
    for vote in election:
        #print(vote[0])
        if vote[0] == winner or vote[0] == Num:
            vote_totals[vote[0]] += 1
        else:
            vote_totals[vote[1]] += 1
    #print("Vote totals:"+str(vote_totals))
    return max(vote_totals.items(), key=lambda x: x[1])[0]
if __name__ == "__main__":
    import doctest
    doctest.testmod()
