import curses
import time
import signal
import sys
import math
def signalHandler(sig, frame):
    curses.endwin()
    sys.exit()
signal.signal(signal.SIGINT, signalHandler)

def rawInput(stdscr, r, c, prompt_string):
    curses.echo()
    stdscr.addstr(r, c, prompt_string)
    stdscr.refresh()
    input = stdscr.getstr(r + 1, c)
    input = str(input)[2:-1]
    return input
def winMsg(stdscr,string,scrollAmount,row,col):
    stdscr.scroll(scrollAmount)
    stdscr.addstr(row,col,string)
    stdscr.refresh()

stdscr=curses.initscr()
rows, cols = stdscr.getmaxyx()
curses.start_color()
curses.init_pair(1, curses.COLOR_BLACK, curses.COLOR_GREEN)
curses.init_pair(2, 17, 8)
curses.init_pair(3, 1, 180)
curses.init_pair(4, 3, 4)
curses.init_pair(5, 1, 2)
inputWin=curses.newwin(2,0,curses.LINES-2,0)
topWin=curses.newwin(1,0,0,0)
midWin=curses.newwin(int(math.ceil(rows/2))-2,0,int(rows/2),0)
leftWin=curses.newwin(int(rows/2)-1,int(cols/2),1,0)
rightWin=curses.newwin(int(rows/2)-1,int(math.ceil(cols/2)),1,int(cols/2))
inputWin.bkgd(' ', curses.color_pair(1))
topWin.bkgd(' ', curses.color_pair(2))
midWin.bkgd(' ', curses.color_pair(3))
leftWin.bkgd(' ', curses.color_pair(4))
rightWin.bkgd(' ', curses.color_pair(5))
topWin.addstr(0,0,str(curses.LINES)+" "+str(curses.COLS))
midWin.addstr(0,0,"hello world")
leftWin.addstr(0,0,"hello world")
rightWin.addstr(0,0,"hello world")
#x=rawInput(inputWin,0,0,"input")
stdscr.refresh()
topWin.refresh()
midWin.refresh()
inputWin.refresh()
leftWin.refresh()
rightWin.refresh()
x=rawInput(inputWin,0,0,"input")
stdscr.getch()
curses.endwin()
