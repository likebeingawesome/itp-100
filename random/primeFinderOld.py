import math
import time
import datetime
#intiallization
tmin=float(input("What is the lowest number you would like to check to see if it is prime?"))
max=float(input("What is the highest number you would like to check to see if it is prime?"))
#Gets the highest and lowest number to be checked
while tmin>max:
    print("The first number you entered is larger than the second. Please renter the numbers.")
    tmin=float(input("What is the lowest number you would like to check to see if it is prime?"))
    max=float(input("What is the highest number you would like to check to see if it is prime?"))
min=tmin
if min.is_integer()==False:
    min=math.floor(min)
if 0<=min<2:
    min=2
    print("1 is neither prime nor composite.")
if min<0:
    min=2
    print("All negative numbers are composite")

start_time=time.time()
#grabs the time in seconds
primeList=[2]
for numChecked in range(int(min),int(max)+1):
    #main for loop
    prime=True
    for n in primeList:
        #print(numChecked/n)
        if numChecked/n == int(numChecked/n) or numChecked/n < 1:
            #checks if the number being checked makes an integer when divided by the number it is being checked with.
            prime=False
            break
            #this break is not neccisary but cuts down on the time the program takes drastically
    if prime==True:
        primeList.append(numChecked)
        #print(str(numChecked)+(" is a prime number"))
        #adds to the prime count
end_time = time.time()
datetime=str(datetime.datetime.now())
f = open(str(datetime)+".txt","w+")
f.write(str(primeList)+"\n")
f.write("Took "+str(end_time-start_time)+" seconds\n")
f.write("Counted "+str(len(primeList))+" Primes\n")
f.close()
#grabs the time in seconds at the end of the program
print(("counted ")+str(len(primeList))+(" prime numbers between ")+str(tmin)+(" and ")+str(max)+(" and took ")+str(end_time-start_time)+(" seconds"))
print("saved log to "+datetime+".txt")
#tells the user how long it took the program to run and how many prime numbers were found.
